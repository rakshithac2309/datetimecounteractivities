package com.example.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.CountDownTimer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class counteractivity extends AppCompatActivity {
    private Button btnStart;
    private TextView locateText;
    private int counter = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counteractivity);
        btnStart = (Button) findViewById(R.id.namebutton);
        locateText = (TextView) findViewById(R.id.textname);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new CountDownTimer(11000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        locateText.setText(String.valueOf(counter));
                        counter--;
                    }
                    public  void onFinish(){
                        counter =10;
                        locateText.setText(String.valueOf(10));
                    }
                }.start();
            }
        });

    }
}
