package com.example.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView counter=(ImageView) findViewById(R.id.counter);
        TextView date=(TextView) findViewById(R.id.date);
        date.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent datentime = new Intent (MainActivity.this,dateactivity.class);
                                        startActivity(datentime);
                                    }
                                }
        );
        counter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent countintent=new Intent(MainActivity.this,counteractivity.class);
                startActivity(countintent);
            }


        });
    }
}
